

## Script status

[![Version 2.1.1-production](https://img.shields.io/badge/version-2.1.1-674172.svg?style=flat-square)](https://plaza.quickbox.io/t/quickbox-readme-md/31)
[![GNU v3.0 License](https://img.shields.io/badge/license-GNU%20v3.0%20License-blue.svg?style=flat-square)](https://github.com/Swizards/QuickBox/blob/master/LICENSE.md)

#### Ubuntu Builds
[![Ubuntu 16.04 Passing](https://img.shields.io/badge/Ubuntu%2016.04-passing-brightgreen.svg?style=flat-square)](https://plaza.quickbox.io/t/quickbox-readme-md/31)

---

###Quick Advisory Notice on QuickBox
---
> ###Heads Up!
This is a purely development repo. If you are attempting to download for functional use, you may encounter plenty of issues. If you are wanting a function/stable script, please visit the [QuickBox Plaza](https://plaza.quickbox.io/t/quickbox-readme-md/31)

####Again, Please note:
> This is being released as a public sandbox... meaning that it is user-contribution driven. Swizards take a great deal of pride in providing quality UI. Enhancement requests and more for the QuickBox Project will not be included in the future unless users feel kind enough to contribute to the repo by pushing requests for their included modifications... basically... it's community driven... simple


### For a full README and CHANGELOG
For a full README and CHANGELOG on QuickBox as well as how to install, please visit the [QuickBox Plaza](https://plaza.quickbox.io/t/quickbox-readme-md/31)

## Before installation
You need to have a Fresh "blank" server installation.
After that access your box using a SSH client, like PuTTY.

---



## How to install
> This script is valid for both VPS & Dedicated Environments.

## NOTICE
> It is highly advisable that QuickBox be installed on Ubuntu 16.04. For more about this, [see here](https://plaza.quickbox.io/t/poll-are-you-using-or-going-to-be-using-ubuntu-16-04/41) and weigh in on your thoughts.

####You must be logged in as root to run this installation.

---
### Ubuntu 16.04

#### Run the following command to grab the latest stable release [Ubunut 16.04 w/ PHP 7 - standalone]
```
apt-get -yqq update; apt-get -yqq upgrade; apt-get -yqq install git curl lsb-release; \
git clone https://QuickBox@bitbucket.org/QuickBox/quickbox.git /root/tmp/QuickBox/; \
cd /root/tmp/QuickBox*; \
bash quickbox.sh
```

#### Run the following command to grab the latest development release [Default partition on / ]
```
apt-get -yqq update; apt-get -yqq upgrade; apt-get -yqq install git curl lsb-release; \
git clone -b development https://QuickBox@bitbucket.org/QuickBox/quickbox.git /root/tmp/QuickBox/; \
cd /root/tmp/QuickBox*; \
bash quickbox.sh
```

#### Run the following command to grab the latest development w/nginx release [Default partition on / ] Experimental (non-functional)
```
apt-get -yqq update; apt-get -yqq upgrade; apt-get -yqq install git curl lsb-release; \
git clone -b development https://QuickBox@bitbucket.org/QuickBox/quickbox.git /root/tmp/QuickBox/; \
cd /root/tmp/QuickBox*; \
bash quickbox_nginx.sh
```

#### Run the following command to grab the latest development release [Default partition on /home ] Experimental (non-functional)
```
apt-get -yqq update; apt-get -yqq upgrade; apt-get -yqq install git curl lsb-release; \
git clone -b development https://QuickBox@bitbucket.org/QuickBox/quickbox.git /root/tmp/QuickBox/; \
cd /root/tmp/QuickBox*; \
bash quickbox_home.sh

```

---


## Commands
After installing you will have access to the following commands to be used directly in terminal

* ~~quickbox~~ - tells you which version Quick Box you are running and shows commands list
  + ``deprecated due to versioning now shows on the dashboard under username dropdown``
* createSeedboxUser - creates a shelled seedbox user
* deleteSeedboxUser - deletes a created seedbox user and their directories
  + **This is permanent, current data will be deleted - you can create them again at any time**
* setdisk - set your disk quota for any given user
* reload - restarts your seedbox services, i.e; rtorrent & irssi
* ~~upgradeBTSync~~ -- upgrades btsync when new version is available
  + `deprecated due to btsync now supplying a reliable and official ppa``
